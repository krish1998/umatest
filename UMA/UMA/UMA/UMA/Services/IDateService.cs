﻿using System;
using System.Collections.Generic;
using System.Text;
using UMA.Models;

namespace UMA.Services
{
    public interface IDateService
    {
        WeekModel GetWeek(DateTime date);

        List<DayModel> GetDayList(DateTime firstDayInWeek, DateTime lastDayInWeek);
    }
}
