﻿
using Prism.Ioc;
using Prism.Unity;
using System;
using UMA.Dialogs;
using UMA.Services;
using UMA.ViewModels;
using UMA.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UMA
{
    public partial class App : PrismApplication
    {
        public static int SelectedIndex = -1;
        //public App()
        //{
        //    InitializeComponent();

        //    MainPage = new MainPage();
        //}
        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.Register<IDateService, DateService>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<MainPage>();
            containerRegistry.RegisterForRegionNavigation<SignUpPage, SignUpViewModel>();
            containerRegistry.RegisterDialog<ErrorDialog, ErrorDialogViewModel>();
            containerRegistry.RegisterForNavigation<VisitDetailsPage>();

            // throw new NotImplementedException();
        }
        //public const string Mypage = "MainPage";
        protected async override void OnInitialized()
        {
            InitializeComponent();
            await NavigationService.NavigateAsync($"/{nameof(NavigationPage)}/{nameof(LoginPage)}");
            // await NavigationService.NavigateAsync("LoginPage");
        }
        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
