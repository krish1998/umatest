﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UMA.Helpers.Validations
{
    public interface IValidaty
    {
        bool IsValid { get; set; }
        bool IsButtonActive { get; set; }
    }
}
