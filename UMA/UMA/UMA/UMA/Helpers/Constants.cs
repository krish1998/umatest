﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UMA.Helpers
{
    public static class Constants
    {
        public static class Errors
        {
            public static string GeneralError = "Something went wrong! Please wait a moment and try again.";
            public static string WrongUserOrPasswordError = "The email or password is incorrect";
        }
    }
}
