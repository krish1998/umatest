﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UMA.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UMA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VisitDetailsPage : ContentPage
    {
        public VisitDetailsPage()
        {
            InitializeComponent();

            Products = new ObservableCollection<Product>
            {
                new Product
                {
                    Title = "Test1",
                    Isvisible =false
                },
                new Product
                {
                    Title = "Test2",
                    Isvisible = false
                },
                new Product
                {
                    Title = "Test3",
                    Isvisible = false
                },
                new Product
                {
                    Title = "Test4",
                    Isvisible =  false
                },
                new Product
                {
                    Title = "Test5",
                    Isvisible = false
                }
            };
            lst.ItemsSource = Products;
        }
        private void UpDateProducts(Product product)
        {

            var Index = Products.IndexOf(product);
            Products.Remove(product);
            Products.Insert(Index, product);

        }
        public void ShoworHiddenProducts(Product product)
        {
            if (_oldProduct == product)
            {
                product.Isvisible = !product.Isvisible;
                UpDateProducts(product);
            }
            else
            {
                if (_oldProduct != null)
                {
                    _oldProduct.Isvisible = false;
                    UpDateProducts(_oldProduct);

                }
                product.Isvisible = true;
                UpDateProducts(product);
            }
            _oldProduct = product;
        }
        private Product _oldProduct;
        public ObservableCollection<Product> Products { get; set; }
        private void ListViewItem_Tabbed(object sender, ItemTappedEventArgs e)
        {
            var product = e.Item as Product;
            ShoworHiddenProducts(product);
        }
    }
}