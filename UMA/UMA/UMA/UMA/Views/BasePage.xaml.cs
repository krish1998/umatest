﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace UMA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BasePage : ContentPage
    {
        public static readonly BindableProperty MainContentProperty = BindableProperty.Create(nameof(MainContent), typeof(View), typeof(BasePage));

        public View MainContent
        {
            get => (View)GetValue(MainContentProperty);
            set => SetValue(MainContentProperty, value);
        }

        public static readonly BindableProperty NavBarContentProperty = BindableProperty.Create(nameof(NavBarContent), typeof(View), typeof(BasePage));

        public View NavBarContent
        {
            get => (View)GetValue(NavBarContentProperty);
            set => SetValue(NavBarContentProperty, value);
        }

        public BasePage()
        {
            InitializeComponent();
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            if (MainContent == null)
                return;

            SetInheritedBindingContext(MainContent, BindingContext);
        }
    }
}
