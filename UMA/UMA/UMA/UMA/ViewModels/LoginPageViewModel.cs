﻿using Prism.Commands;
using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Input;
using UMA.Dialogs;
using UMA.Helpers;
using UMA.Helpers.Validations;
using UMA.Helpers.Validations.Rules;
using UMA.Views;
using Xamarin.CommunityToolkit.UI.Views;
using Xamarin.Forms;

namespace UMA.ViewModels
{
    public class LoginPageViewModel : BaseRegionViewModel
    {
        #region Private & Protected

        private IDialogService _dialogService;

        #endregion

        #region Properties

        public ValidatableObject<string> Email { get; set; }
        public ValidatableObject<string> Password { get; set; }

        #endregion

        #region Commands

        public DelegateCommand LoginCommand { get; set; }
        public ICommand ValidateCommand { get; set; }

        #endregion

        #region Constructors

        public LoginPageViewModel(DialogService dialogService, INavigationService navigationService)
            : base(navigationService)
        {
            _dialogService = dialogService;

            LoginCommand = new DelegateCommand(LoginCommandHandler);

            ValidateCommand = new Command<string>(ValidateCommandHandler);

            AddValidations();
        }

        #endregion

        #region Validation Handlers

        private void ValidateCommandHandler(string field)
        {
            switch (field)
            {
                case "email": Email.Validate(); break;
                case "password": Password.Validate(); break;
            }
        }

        #endregion

        #region Command Handlers

        private async void LoginCommandHandler()
        {
            try
            {
                MainState = LayoutState.Loading;
                if (ValidateLoginData())
                {
                    // var auth = DependencyService.Get<IFirebaseAuthentication>();
                    // var user = await auth.LoginWithEmailAndPassword(Email.Value, Password.Value);
                    var user = Email.Value;
                    if (user != null)
                    {
                        string data = string.Empty;
                        //var param = new DialogParameters()
                        //{
                        //    { "message", Constants.Errors.WrongUserOrPasswordError }
                        //};
                        //_dialogService.ShowDialog(nameof(ErrorDialog), param);
                        // ClearAuthData();
                        //var param = new DialogParameters()
                        //{
                        //    { "message", Constants.Errors.WrongUserOrPasswordError }
                        //};
                        //_dialogService.ShowDialog(nameof(ErrorDialog), param);
                        //  await _navigationService.NavigateAsync(nameof(AuthPage));
                        await _navigationService.NavigateAsync(nameof(VisitDetailsPage));
                    }
                    else
                    {
                        var param = new DialogParameters()
                        {
                            { "message", Constants.Errors.WrongUserOrPasswordError }
                        };
                        _dialogService.ShowDialog(nameof(ErrorDialog), param);
                    }
                }
            }
            catch (Exception ex)
            {
                var param = new DialogParameters()
                {
                    { "message", Constants.Errors.GeneralError }
                };
                _dialogService.ShowDialog(nameof(ErrorDialog), param);
                Debug.WriteLine(ex);
            }
            finally
            {
                // MainState = LayoutState.None;
            }
        }

        #endregion

        #region Private Functionality

        private void AddValidations()
        {
            Email = new ValidatableObject<string>();
            Password = new ValidatableObject<string>();

            //Email.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "A email is required." });
            //Email.Validations.Add(new IsEmailRule<string> { ValidationMessage = "Email format is not correct" });
            //Password.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "A password is required." });
        }

        private bool ValidateLoginData()
        {
            if (Email.IsValid == false ||
                Password.IsValid == false)
                return false;
            return true;
        }

        private void ClearAuthData()
        {
            Email.Value = Password.Value = string.Empty;
        }

        #endregion
    }
}
