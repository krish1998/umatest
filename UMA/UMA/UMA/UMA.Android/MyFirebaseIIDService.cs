﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Firebase.Iid;
using Plugin.DeviceInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMA.Droid
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    [Obsolete]
    public class MyFirebaseIIDService : FirebaseInstanceIdService
    {

        const string TAG = "MyFirebaseIIDService";

        [Obsolete]
        public override void OnTokenRefresh()
        {
            var refreshedToken = FirebaseInstanceId.Instance.Token;
            Log.Debug(TAG, "Refreshed token: " + refreshedToken);
            var deviceId = CrossDeviceInfo.Current.Id;
            SendRegistrationToServer(refreshedToken);
        }
        void SendRegistrationToServer(string token)
        {
            // Add custom implementation, as needed.
        }




        /*  public override void OnTokenRefresh()
          {
              var refreshedToken = FirebaseInstanceId.Instance.Token;
               App.deviceToken = FirebaseInstanceId.Instance.Token;
              Log.Debug(TAG, "Refreshed token: " + refreshedToken);

              var deviceId = CrossDeviceInfo.Current.Id;
              WebApi objApi = new WebApi();
              var status=objApi.NotificationAndroid(refreshedToken,deviceId);

             // Toast.MakeText(this,status, ToastLength.Short).Show();

              SendRegistrationToServer(refreshedToken,deviceId);
          }

          void SendRegistrationToServer(string token,string deviceId)
          {

          }*/
    }
}