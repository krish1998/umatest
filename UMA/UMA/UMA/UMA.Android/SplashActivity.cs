﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UMA.Droid
{
    /// <summary>
    /// Class to handle splash screen activity
    /// </summary>
    [Activity(Theme = "@style/MainTheme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {
        /// <summary>
        /// Method called when activity is created
        /// </summary>
        /// <param name="savedInstanceState"></param>
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            InvokeMainActivity();
        }

        /// <summary>
        /// Method to invoke main activity
        /// </summary>
        private void InvokeMainActivity()
        {
            var mainActivityIntent = new Intent(this, typeof(MainActivity));

            if (Intent.Extras != null)
            {
                mainActivityIntent.PutExtras(Intent.Extras);
            }

            mainActivityIntent.AddFlags(ActivityFlags.NoAnimation);

            Task.Run(() =>
            {
                Thread.Sleep(500);
                RunOnUiThread(() =>
                {
                    StartActivity(mainActivityIntent);
                });
            });
        }
    }
}