﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMA.Droid
{

    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        public override void OnMessageReceived(RemoteMessage message)
        {
            base.OnMessageReceived(message);
            SendNotificatios(message.GetNotification().Body, message.GetNotification().Title);
        }
        public void SendNotificatios(string body, string Header)
        {
            try
            {
                Notification.Builder builder = new Notification.Builder(this);
                //builder.SetSmallIcon(Resource.Drawable.icon);
                // builder.SetSmallIcon(Resource.Drawable.Icon);
                var intent = new Intent(this, typeof(MainActivity));
                intent.AddFlags(ActivityFlags.ClearTop);
                PendingIntent pendingIntent = PendingIntent.GetActivity(this, 0, intent, 0);
                builder.SetContentIntent(pendingIntent);
                //builder.SetLargeIcon(BitmapFactory.DecodeResource(Resources, Resource.Drawable.Alaram));
                builder.SetContentTitle(Header);
                builder.SetContentText(body);
                builder.SetDefaults(NotificationDefaults.Sound);
                builder.SetAutoCancel(true);
                NotificationManager notificationManager = (NotificationManager)GetSystemService(NotificationService);
                notificationManager.Notify(1, builder.Build());
            }
            catch (Exception ex)
            {

            }
        }
    }
}