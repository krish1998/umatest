﻿using System;
using System.Text.RegularExpressions;

namespace UMA.UITest.Extensions
{
    public static class StringExtension
    {
        public static bool ValidateEmail(this string email)
        {
            Regex EmailRegex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            if (string.IsNullOrWhiteSpace(email))
                return false;

            return EmailRegex.IsMatch(email);
        }
    }
}
