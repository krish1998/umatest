﻿using System.Linq;
using NUnit.Framework;
using UMA.Models;
using UMA.UITest.Mocks.Login;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UMA.UITest.Tests
{
    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class LoginMockTest
    {
        public static IApp app;
        Platform platform;

        public ILoginMockService LoginServiceMock;

        public LoginMockTest(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            LoginServiceMock = new LoginMockService();
            app = AppInitializer.StartApp(platform);
        }

        //[Fact]
        public void ClassConstructor_IntialParameterIsNotNull_NotThrowException()
        {

        }

       // [Fact]
        public void InitializeCommands_CorrectInitialization()
        {


        }

        [Test]
        public void VerifyInValidLoginTestCase()
        {
            app.WaitForElement(x => x.Marked("EmailEntry"));
            app.Tap("EmailEntry");
            app.EnterText("test@gmail.com");
            app.DismissKeyboard();
            var emailEntry = app.Query(c => c.Id("EmailEntry"));

            app.WaitForElement(x => x.Marked("PasswordEntry"));
            app.Tap("PasswordEntry");
            app.EnterText("test123");
            app.DismissKeyboard();
            var passwordEntry = app.Query(c => c.Id("PasswordEntry"));

            //app.WaitForElement(x => x.Marked("RememberMeCheckbox"));
            //app.Tap("RememberMeCheckbox");
            //var rememberCheckbox = app.Query(c => c.Id("RememberMeCheckbox"));

            app.Tap(c => c.Button("LoginBtn"));

            User user = new User();
            user.Email = emailEntry[0].Text;
            user.Password = passwordEntry[0].Text;
            user.IsRememberMe = true;

            var result = LoginServiceMock.LoginUser(user);

            if (!result.Result.Item1)
            {
                AppResult[] results1 = app.WaitForElement(c => c.Text(result.Result.Item2));
                NUnit.Framework.Assert.IsTrue(results1.Any(), result.Result.Item2);
            }

            AppResult[] results = app.WaitForElement(c => c.Text("Login Successfully"));
            NUnit.Framework.Assert.IsTrue(results.Any(), "Login Test case failed");
        }

        [Test]
        public void EmptyEntriesTestCase()
        {
            app.WaitForElement(x => x.Marked("EmailEntry"));
            var emailEntry = app.Query(c => c.Id("EmailEntry"));

            app.WaitForElement(x => x.Marked("PasswordEntry"));
            var passwordEntry = app.Query(c => c.Id("PasswordEntry"));

            app.Tap(c => c.Button("LoginBtn"));

            User user = new User();
            user.Email = emailEntry[0].Text;
            user.Password = passwordEntry[0].Text;
            user.IsRememberMe = true;

            var result = LoginServiceMock.LoginUser(user);

            if (!result.Result.Item1)
            {
                AppResult[] results1 = app.WaitForElement(c => c.Text(result.Result.Item2));
                NUnit.Framework.Assert.IsTrue(results1.Any(), result.Result.Item2);
            }

            AppResult[] results = app.WaitForElement(c => c.Text("Login Successfully"));
            NUnit.Framework.Assert.IsTrue(results.Any(), "Login Test case failed");
        }

        [Test]
        public void VerifyValidLoginTestCase()
        {
            app.WaitForElement(x => x.Marked("EmailEntry"));
            app.Tap("EmailEntry");
            app.EnterText("hmahajan@hhaexchange.com");
            app.DismissKeyboard();
            var emailEntry = app.Query(c => c.Id("EmailEntry"));

            app.WaitForElement(x => x.Marked("PasswordEntry"));
            app.Tap("PasswordEntry");
            app.EnterText("12345678");
            app.DismissKeyboard();
            var passwordEntry = app.Query(c => c.Id("PasswordEntry"));

            app.Tap(c => c.Button("LoginBtn"));

            User user = new User();
            user.Email = emailEntry[0].Text;
            user.Password = passwordEntry[0].Text;
            user.IsRememberMe = true;

            var result = LoginServiceMock.LoginUser(user);

            if (!result.Result.Item1)
            {
                AppResult[] results1 = app.WaitForElement(c => c.Text(result.Result.Item2));
                NUnit.Framework.Assert.IsTrue(results1.Any(), result.Result.Item2);
            }

            AppResult[] results = app.WaitForElement(c => c.Text("Login Successfully"));
            NUnit.Framework.Assert.IsTrue(results.Any(), "Login Test case failed");
        }

        [Test]
        public void EmailValidateRegexTestCase()
        {
            app.WaitForElement(x => x.Marked("EmailEntry"));
            app.Tap("EmailEntry");
            app.EnterText("hmahajan@");
            app.DismissKeyboard();
            var emailEntry = app.Query(c => c.Id("EmailEntry"));

            app.WaitForElement(x => x.Marked("PasswordEntry"));
            app.Tap("PasswordEntry");
            app.EnterText("12345678");
            app.DismissKeyboard();
            var passwordEntry = app.Query(c => c.Id("PasswordEntry"));

            app.Tap(c => c.Button("LoginBtn"));

            User user = new User();
            user.Email = emailEntry[0].Text;
            user.Password = passwordEntry[0].Text;
            user.IsRememberMe = true;

            var result = LoginServiceMock.LoginUser(user);

            if (!result.Result.Item1)
            {
                AppResult[] results1 = app.WaitForElement(c => c.Text(result.Result.Item2));
                NUnit.Framework.Assert.IsTrue(results1.Any(), result.Result.Item2);
            }

            AppResult[] results = app.WaitForElement(c => c.Text("Login Successfully"));
            NUnit.Framework.Assert.IsTrue(results.Any(), "LLogin Test case failed");
        }

        //public string GetEntryFieldText(string Entity)
        //{
        //    Func<AppQuery, AppQuery> applicationUrlEntryField = x => x.Marked(Entity);

        //    BaseTest.GetApp().WaitForElement(applicationUrlEntryField);

        //    var applicationUrlEntryFieldQuery = BaseTest.GetApp().Query(applicationUrlEntryField);

        //    return applicationUrlEntryFieldQuery?.FirstOrDefault()?.Text ?? string.Empty;
        //}
    }
}
