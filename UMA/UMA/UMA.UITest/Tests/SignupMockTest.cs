﻿using System;
using System.Globalization;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UMA.UITest.Tests
{
    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class SignupMockTest
    {
        IApp app;
        Platform platform;

        public SignupMockTest(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        [Test]
        public void BackToSignInPageTestCase()
        {
            app.WaitForElement(c => c.Marked("SignInPage"));
            app.Tap(c => c.Button("SignupBtn"));
            app.WaitForElement(c => c.Marked("SignUpPage"));
            app.Tap(c => c.Button("CancelBtn"));
            app.WaitForElement(c => c.Marked("SignInPage"));
            AppResult[] results = app.WaitForElement(c => c.Text("Email"));
            app.Screenshot("SignIn screen");
            Assert.IsTrue(results.Any(), "signin screen open test case failed!!!");
        }

        [Test]
        public void EmptyEntriesTestCase()
        {
            app.WaitForElement(c => c.Marked("SignInPage"));
            app.Tap(c => c.Button("SignupBtn"));
            app.WaitForElement(c => c.Marked("SignUpPage"));
            app.Tap(c => c.Button("SignUpBtn"));
            AppResult[] results = app.WaitForElement(c => c.Text("One or more fields are empty."));
            Assert.IsTrue(results.Any(), "Empty Entries Test case failed!!!");
        }

        [Test]
        public void PasswordDoesNotMatchTestCase()
        {
            app.WaitForElement(c => c.Marked("SignInPage"));
            app.Tap(c => c.Button("SignupBtn"));
            app.WaitForElement(c => c.Marked("SignUpPage"));

            app.Tap("FirstNameEntry");
            app.EnterText("Himanshu");
            app.DismissKeyboard();

            app.Tap("LastNameEntry");
            app.EnterText("Mahajan");
            app.DismissKeyboard();

            app.Tap("GenderEntry");
            app.EnterText("Male");
            app.DismissKeyboard();

            app.Tap(c => c.Marked("BirthdayDatePicker"));
            SetDatePicker(new DateTime(1993, 7, 20));

            app.Tap("SignupEmailEntry");
            app.EnterText("hmahajan@hhaexchange.com");
            app.DismissKeyboard();

            app.Tap("PhoneEntry");
            app.EnterText("7888560889");
            app.DismissKeyboard();

            app.Tap("SignupPasswordEntry");
            app.EnterText("12345678");
            app.DismissKeyboard();

            app.Tap("ConfirmPasswordEntry");
            app.EnterText("Test123");
            app.DismissKeyboard();

            app.Tap(c => c.Button("SignUpBtn"));
            AppResult[] results = app.WaitForElement(c => c.Text("Password doesnot match with Confirm Password"));
            Assert.IsTrue(results.Any(), "Password match test case failed!!!");
        }

        [Test]
        public void GoToMainPageTestCase()
        {
            app.WaitForElement(c => c.Marked("SignInPage"));
            app.Tap(c => c.Button("SignupBtn"));
            app.WaitForElement(c => c.Marked("SignUpPage"));

            app.Tap("FirstNameEntry");
            app.EnterText("Himanshu");
            app.DismissKeyboard();

            app.Tap("LastNameEntry");
            app.EnterText("Mahajan");
            app.DismissKeyboard();

            app.Tap("GenderEntry");
            app.EnterText("Male");
            app.DismissKeyboard();

            app.Tap(c => c.Marked("BirthdayDatePicker"));
            SetDatePicker(new DateTime(1993, 7, 20));

            app.Tap("SignupEmailEntry");
            app.EnterText("hmahajan@hhaexchange.com");
            app.DismissKeyboard();

            app.Tap("PhoneEntry");
            app.EnterText("7888560889");
            app.DismissKeyboard();

            app.Tap("SignupPasswordEntry");
            app.EnterText("12345678");
            app.DismissKeyboard();

            app.Tap("ConfirmPasswordEntry");
            app.EnterText("12345678");
            app.DismissKeyboard();

            app.Tap(c => c.Button("SignUpBtn"));
            app.WaitForElement(c => c.Marked("MainPage"));
            app.Screenshot("Main screen");
            AppResult[] results = app.WaitForElement(c => c.Text("Login Successfully"));
            Assert.IsTrue(results.Any(), "Signup Test case failed!!!");
        }

        [Test]
        public void PhoneNumberValidateUptoTenDigitsTestCase()
        {
            app.WaitForElement(c => c.Marked("SignInPage"));
            app.Tap(c => c.Button("SignupBtn"));
            app.WaitForElement(c => c.Marked("SignUpPage"));

            app.Tap("FirstNameEntry");
            app.EnterText("Himanshu");
            app.DismissKeyboard();

            app.Tap("LastNameEntry");
            app.EnterText("Mahajan");
            app.DismissKeyboard();

            app.Tap("GenderEntry");
            app.EnterText("Male");
            app.DismissKeyboard();

            app.Tap(c => c.Marked("BirthdayDatePicker"));
            SetDatePicker(new DateTime(1993, 7, 20));

            app.Tap("SignupEmailEntry");
            app.EnterText("hmahajan@hhaexchange.com");
            app.DismissKeyboard();

            app.Tap("PhoneEntry");
            app.EnterText("78885608899991");
            app.DismissKeyboard();

            app.Tap("SignupPasswordEntry");
            app.EnterText("12345678");
            app.DismissKeyboard();

            app.Tap("ConfirmPasswordEntry");
            app.EnterText("12345678");
            app.DismissKeyboard();

            app.Tap(c => c.Button("SignUpBtn"));
            AppResult[] results = app.WaitForElement(c => c.Text("Phone Number not more than 10 digits"));
            Assert.IsTrue(results.Any(), "Phone Number valid Test case failed!!!");
        }

        [Test]
        public void EmailValidateRegexTestCase()
        {
            app.WaitForElement(c => c.Marked("SignInPage"));
            app.Tap(c => c.Button("SignupBtn"));
            app.WaitForElement(c => c.Marked("SignUpPage"));

            app.Tap("FirstNameEntry");
            app.EnterText("Himanshu");
            app.DismissKeyboard();

            app.Tap("LastNameEntry");
            app.EnterText("Mahajan");
            app.DismissKeyboard();

            app.Tap("GenderEntry");
            app.EnterText("Male");
            app.DismissKeyboard();

            app.Tap(c => c.Marked("BirthdayDatePicker"));
            SetDatePicker(new DateTime(1993, 7, 20));

            app.Tap("SignupEmailEntry");
            app.EnterText("hmahajan@");
            app.DismissKeyboard();

            app.Tap("PhoneEntry");
            app.EnterText("7888560889");
            app.DismissKeyboard();

            app.Tap("SignupPasswordEntry");
            app.EnterText("12345678");
            app.DismissKeyboard();

            app.Tap("ConfirmPasswordEntry");
            app.EnterText("12345678");
            app.DismissKeyboard();

            app.Tap(c => c.Button("SignUpBtn"));
            AppResult[] results = app.WaitForElement(c => c.Text("Email is not valid"));
            Assert.IsTrue(results.Any(), "Email is not valid Test case failed!!!");
        }

        public void SetDatePicker(DateTime date)
        {
            // needed to tap & thus select the items
            string month = DateTimeFormatInfo.CurrentInfo.GetMonthName(date.Month);
            string day = date.Day.ToString();
            string year = date.Year.ToString();

            if (platform == Platform.iOS)
            {
                //Invoke the native method selectRow() 
                app.Query(x => x.Class("UIPickerView").Invoke("selectRow", date.Month, "inComponent", 0, "animated", true)); // brings month in scope
                app.Tap(month); // actually selects month

                app.Query(x => x.Class("UIPickerView").Invoke("selectRow", date.Day, "inComponent", 1, "animated", true));
                app.Tap(day);

                app.Query(x => x.Class("UIPickerView").Invoke("selectRow", date.Year, "inComponent", 2, "animated", true));
                app.Tap(year);
            }
            else // Android
            {
                var dateTime = new DateTime(1993, 7, 20);
                app.WaitForElement(x => x.Class("DatePicker"));
                app.Query(x => x.Class("DatePicker").Invoke("updateDate", dateTime.Year, dateTime.Month - 1, dateTime.Day));
                app.Tap("OK");
                //app.Query(x => x.Class("DatePicker").Invoke("updateDate", date.Year, date.Month - 1, date.Day));
            }
        }
    }
}
