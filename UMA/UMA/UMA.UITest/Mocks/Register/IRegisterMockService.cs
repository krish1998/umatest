﻿using System;
using System.Threading.Tasks;
using UMA.Models;

namespace UMA.UITest.Mocks.Register
{
    public interface IRegisterMockService
    {
        Task<Tuple<bool, string>> RegisterUser(User user);
    }
}
