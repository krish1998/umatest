﻿using System;
using System.Threading.Tasks;
using UMA.Models;
using UMA.UITest.Extensions;

namespace UMA.UITest.Mocks.Register
{
    public class RegisterMockService : IRegisterMockService
    {
        public async Task<Tuple<bool, string>> RegisterUser(User user)
        {
            await Task.Delay(4000);
            if (string.IsNullOrWhiteSpace(user.FirstName) || string.IsNullOrWhiteSpace(user.LastName)
                || string.IsNullOrWhiteSpace(user.Gender) || string.IsNullOrWhiteSpace(user.Birthday)
                || string.IsNullOrWhiteSpace(user.Email) || string.IsNullOrWhiteSpace(user.Phone)
                || string.IsNullOrWhiteSpace(user.Password) || string.IsNullOrWhiteSpace(user.ConfirmPassword))
                return Tuple.Create(false, "One or more fields are empty.");
            else if (!user.Email.ValidateEmail())
                return Tuple.Create(false, "Email is not valid");
            else if (user.Phone.Length != 10)
                return Tuple.Create(false, "Phone Number not more than 10 digits");
            else if (user.Password != user.ConfirmPassword)
                return Tuple.Create(false, "Password doesnot match with Confirm Password");
            return Tuple.Create(true, "Register Successfully"); ;
        }
    }
}
