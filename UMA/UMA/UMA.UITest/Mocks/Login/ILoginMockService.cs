﻿using System;
using System.Threading.Tasks;
using UMA.Models;

namespace UMA.UITest.Mocks.Login
{
    public interface ILoginMockService
    {
        Task<Tuple<bool, string>> LoginUser(User user);
    }
}
