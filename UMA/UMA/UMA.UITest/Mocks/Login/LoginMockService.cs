﻿using System;
using System.Threading.Tasks;
using UMA.Models;
using UMA.UITest.Extensions;

namespace UMA.UITest.Mocks.Login
{
    public class LoginMockService : ILoginMockService
    {
        public async Task<Tuple<bool, string>> LoginUser(User user)
        {
            await Task.Delay(4000);
            if (string.IsNullOrWhiteSpace(user.Email) || string.IsNullOrWhiteSpace(user.Password))
                return Tuple.Create(false, "One or more fields are empty.");
            else if (!user.Email.ValidateEmail())
                return Tuple.Create(false, "Email is not valid");
            else if ((user.Email != "Hmahajan@hhaexchange.com" && user.Email != "hmahajan@hhaexchange.com") || user.Password != "12345678")
                return Tuple.Create(false, "Wrong email or password.");
            return Tuple.Create(true, "Login Successfully"); ;
        }
    }
}
